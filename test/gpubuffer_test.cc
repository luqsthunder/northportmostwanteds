#define BOOST_TEST_MODULE GPUBUFFER_TEST
#include <boost/test/unit_test.hpp>

#include <SDL.h>
#include <glbinding/Binding.h>

namespace bt = boost::unit_test;

#include "render_targets/gl4+/gl4xrendertarget.h"

BOOST_AUTO_TEST_CASE( constructors )
{
  try {
    npmw::gl4p::GPUBuffer<uint32_t> a{{1, 2, 3, 4, 5, 6, 7}};
    std::vector<unsigned> vecInd{1, 2, 3, 4, 5, 6, 7};
    npmw::gl4p::GPUBuffer<uint32_t> b{vecInd};
  } catch(...) {
    BOOST_TEST(false);
  }

  BOOST_TEST(true);
};

BOOST_AUTO_TEST_CASE( test2 ) {

  if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    SDL_Log("unable to init SDL2: %s", SDL_GetError());
    BOOST_TEST(false);
  }

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

  SDL_Window *window = SDL_CreateWindow("NPMW",
                                        SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED,
                                        1366, 768,
                                        SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN);
  SDL_GLContext glcontext = SDL_GL_CreateContext(window);

  glbinding::Binding::initialize([](const char *a) {
    return(void(*)())SDL_GL_GetProcAddress(a);
  }, true);

  std::vector<uint32_t> vecInd{1, 2, 3, 4, 5, 6, 7};
  npmw::gl4p::GPUBuffer<uint32_t> buffer{7};

  uint32_t cont = 1;
  for(auto &it : buffer) {
    it = cont++;
  }
  SDL_GL_DeleteContext(glcontext);
  SDL_DestroyWindow(window);

  BOOST_TEST(true);
}