#version 330 core
layout (location = 0)in vec2 _pos;
layout (location = 1)in vec2 _tex;
layout (location = 2)in vec3 _color;

uniform mat4 _projCam;

out vec4 out_color;

void main() {
  gl_Position = _projCam * vec4(_pos, 0.0, 1.0);
  out_color = vec4(_color, 1.0);
}