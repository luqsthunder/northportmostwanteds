#ifndef NPMW_UTILS_HH
#define NPMW_UTILS_HH

#include <algorithm>
#include <type_traits>

#ifdef _WIN32
namespace std
{
#include <stdio.h>
  template<typename T>
  std::string
  to_string(T a);

  template<typename T>
  std::string
  to_string(typename std::enable_if<std::is_integral<T>::value, T> a)
  {
    char c[16];
    sprintf(c, "%d", a);
    return std::string(c);
  }
}
#endif

namespace npmw
{

template<typename T>
class EmptyDeleter
{
public:
  EmptyDeleter() { }

  void operator()(T * ptr) { }
};

template<typename T>
T clamp(T lowerBound, T upperBound, T value)
{
  return std::max(lowerBound, std::min(value, upperBound));
}
}
#endif //NPMW_UTILS_HH
