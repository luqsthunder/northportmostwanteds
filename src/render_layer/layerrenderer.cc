#include "layerrenderer.h"

#include <vector>
#include <algorithm>

#include <boost/range.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/irange.hpp>

using namespace npmw;

//!
//! @brief cria os vertices para um tilemap de Linhas X Colunas
//!
//! Cria os vertices para um tilemap levando em considera��o que os vertices
//! s�o normalizados entre -1 e 1, para as coordenadas X e Y.
//!
//! @param rows quantidade de linhas 
//! @param cols quantidade de colunas
//!
//! @return vertices para um tilemap 
std::vector<npmw::dx::Vertex> 
mapLayerVertices(uint32_t rows, uint32_t cols) {
  float hTileInc= 2.f / (float)cols;
  float vTileInc= 2.f / (float)rows;

  using npmw::dx::Vertex;
  std::vector<Vertex> verts;
  // observe que para 2 tiles(1 linha, 2 colunas) temos 3 colunas de vertices
  // e 2 linhas de vertices. 
  // 0---14---5
  // | \ || \ |
  // 2---36---7
  verts.resize(rows * cols * 4);
  size_t idx;
  for(size_t y = 0; y < rows; ++y) {
    for(size_t x = 0; x < cols; ++x) {
      idx = (x + y * cols) * 4;

      verts[idx]    .pos({x * hTileInc - 1.f, y * vTileInc - 1.f});
      verts[idx + 1].pos({(x + 1) * hTileInc - 1.f, y * vTileInc - 1.f});

      verts[idx + 2].pos({x * hTileInc - 1.f, (y + 1) * vTileInc - 1.f});
      verts[idx + 3].pos({(x + 1) * hTileInc - 1.f, (y + 1) * vTileInc - 1.f});
    }
  }

  return verts;
}

std::vector<WORD>
mapLayerIndices(uint32_t rows, uint32_t cols) {
  std::vector<WORD> mapIndices;
  mapIndices.resize(rows * cols * 6);
  size_t indice;
  WORD vertIndice = 0;
  for(size_t y = 0; y < rows; ++y) {
    for(size_t x = 0; x < cols; ++x) {
      // indices for clock-wise vertices
      // 0---1 4---5
      // | \ | | \ |
      // 2---3 6---7
      // so first tile will be, 3 2 0 - 3 0 1

      indice = (x + y * cols) * 6;

      mapIndices[indice]     = vertIndice + 3;
      mapIndices[indice + 1] = vertIndice + 2;
      mapIndices[indice + 2] = vertIndice;

      mapIndices[indice + 3] = vertIndice + 3;
      mapIndices[indice + 4] = vertIndice;
      mapIndices[indice + 5] = vertIndice + 1;

      vertIndice += 4;
    }
  }

  return mapIndices;
}

std::vector<npmw::dx::TexVertex>
mapLayerTilesTexture(uint32_t rows, uint32_t cols, IDirect3DTexture9 *tex) {
  D3DSURFACE_DESC texDesc;
  tex->GetLevelDesc(0, &texDesc);

  float hInc = 1.f / 480.f;
  float vInc = 1.f / 256.f;
  using npmw::dx::TexVertex;
  std::vector<TexVertex> texCoords;

  texCoords.resize(rows * cols * 4);
  size_t idx;
  for(size_t y = 0; y < rows; ++y) {
    for(size_t x = 0; x < cols; ++x) {
      idx = (x + y * cols) * 4;

      texCoords[idx + 3]     = {0.f, 0.f};
      texCoords[idx + 2] = {hInc * 16.f, 0.f};

      texCoords[idx + 1] = {0.f, vInc * 16.f};
      texCoords[idx] = {hInc * 16.f, vInc * 16.f};
    }
  }

  return texCoords;
}

LayerRenderer::LayerRenderer(uint32_t rows, uint32_t cols,
                             npmw::RectPixel viewport,
                             IDirect3DTexture9 ** const texture,
                             npmw::dx::DXRenderTarget &renderTarget)
  : DXRenderable(mapLayerVertices(rows + 2, cols + 2),
                 mapLayerTilesTexture(rows + 2, cols + 2, *texture),
                 texture,
                 mapLayerIndices(rows + 2, cols + 2),
                 renderTarget) {
  ap_transformStack->LoadIdentity();
  ap_transformStack->Scale((FLOAT)((cols + 2) * npmw::tileSize.width / 2),
                           (FLOAT)((rows + 2) * npmw::tileSize.width / 2),
                           0.f);
  ap_transformStack->Translate((FLOAT)viewport.x() + (viewport.width / 2.f),
                               (FLOAT)viewport.y() + (viewport.height/ 2.f), 
                               0.f);
}
