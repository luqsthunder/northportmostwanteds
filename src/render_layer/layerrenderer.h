#pragma once

#include <vector>

#include "render_targets/dxd3/dxrendertarget.h"
#include "render_targets/dxd3/dxrenderable.h"

#include "defs.h"
#include "gametime.h"
#include "tileset.h"

namespace npmw
{

class Tile;

//!
//!
class LayerRenderer : public npmw::dx::DXRenderable
{
public:
  //!
  //! 
  //!
  //!
  LayerRenderer(uint32_t rows, uint32_t cols,
                npmw::RectPixel viewportSize,
                IDirect3DTexture9 ** const texture,
                npmw::dx::DXRenderTarget &renderTarget);
};

}
