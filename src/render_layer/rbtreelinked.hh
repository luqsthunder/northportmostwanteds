#ifndef __NPMW_RB_TREE_LINKED_HH
#define __NPMW_RB_TREE_LINKED_HH

#include <vector>

namespace npmw
{

template<typename T>
class Node
{
public:
  Node(const T& data, Node* prev, Node* next);

  Node* const next;
  Node* const prev;
  T data;
};

class RBTreeLinked
{
public:
  std::vector<int> _buffer;
};

}

#endif
