#pragma once

#include <iostream>
#include <memory>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>

#include <stdexcept>

#include "defs.h"
#include "gametime.h"

namespace npmw
{

//!
//! classe para os tiles 
//!
class Tile
{
public:
  Tile();

  Tile(std::shared_ptr<sf::Texture> tilesetTexture);

  Tile(std::shared_ptr<sf::Texture> tilesetTexture, 
       sf::Vector2f texPosInTiles);

  Tile(std::shared_ptr<sf::Texture> tilesetTexture, uint32_t frequency, 
       uint32_t frames, sf::Vector2f texPosInTiles);

  virtual void update(const npmw::GameTime &gameTime);

  virtual sf::Vector2f currentFramePos() const;

  sf::Vector2f firstFramePos() const;

  uint32_t frames() const;
  void frames(uint32_t frame);

  virtual const Vec2Pixel size() const;

  std::shared_ptr<sf::Texture> texture() const;

  void frequency(uint32_t freq);
  uint32_t frequency() const;
  
private:
  void updateQuad(sf::Vector2f posInTiles);
  uint32_t _frames;
  uint32_t _frequency;
  uint32_t _freqCount;
  uint32_t _currentFrame;
  sf::Vector2f _firstTileFrame;

  std::shared_ptr<sf::Texture> _tilesetTexture;
};

}
