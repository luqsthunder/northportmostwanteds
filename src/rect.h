#ifndef NPMW_RECT_H
#define NPMW_RECT_H

#include <SFML/Graphics/Rect.hpp>
#include <type_traits>
#include <iostream>

namespace npmw
{

template<typename T>
class Rect : public sf::Rect<T>
{
public:
  Rect() : sf::Rect<T>() { }
  Rect(T x, T y, T w, T h) : sf::Rect<T>(x, y, w, h) { }
  Rect(sf::Vector2<T> pos, sf::Vector2<T> size) : sf::Rect<T> (pos, size) { }

  Rect(std::initializer_list<T> list) : sf::Rect<T>(0, 0, 0, 0)
  {
    switch(list.size())
    {
      case 4:
        sf::Rect<T>::height = *(list.begin() + 3);
      case 3:
        sf::Rect<T>::width = *(list.begin() + 2);
      case 2:
        sf::Rect<T>::top = *(list.begin() + 1);
      case 1:
        sf::Rect<T>::left = *(list.begin());
      default:
        break;
    }
  }

  inline T& x()
  {
    return sf::Rect<T>::left;
  }

  inline T& y()
  {
    return sf::Rect<T>::top;
  }

  inline const T& x() const
  {
    return sf::Rect<T>::left;
  }

  inline const T& y() const
  {
    return sf::Rect<T>::top;
  }
};

template<typename T>
std::ostream& operator<<(std::ostream &out, const npmw::Rect<T> &rect);

}

template<typename T>
std::ostream& npmw::operator<<(std::ostream &out, const npmw::Rect<T> &rect)
{
  return out << "[x:" << rect.x() << " y:" << rect.y()
             << " width:" << rect.width
             << " height:" << rect.height << "]";
}


#endif
