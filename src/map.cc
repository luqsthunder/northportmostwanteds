#include "map.h"

#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>

#include "tile.h"

using namespace npmw;

Map::Map(const std::string &path, const npmw::SizeU32 tilesetSize) {
  std::ifstream file{path};

  rapidjson::IStreamWrapper stream{file};

  rapidjson::Document doc;
  doc.ParseStream(stream);

  _width = doc["width"].GetUint();
  _heigth = doc["height"].GetUint();

  auto layersJson = doc["layers"].GetArray();
  auto layerLength = layersJson.Size();

  _layers.resize(layerLength);
  _layerDepths.resize(layerLength);

  rapidjson::Value layerAuxValue;
  int layerDepth;

  std::string tilesetId;
  tilesetId = doc["tilesets"].GetArray()[0]["image"].GetString();

  for(size_t i = 0; i < (size_t)layerLength; ++i) {
    layerAuxValue = layersJson[i]["data"].GetArray();
    layerDepth = layersJson[i]["properties"]["layer"].GetInt();

    _layerDepths[i]= (uint32_t)layerDepth;
    _layers[i].reserve(_width * _heigth);

    for(rapidjson::SizeType it = 0; it < layerAuxValue.Size(); ++it) {
      if(!layerAuxValue[it].IsInt())
        continue;

      auto tileValue = layerAuxValue[it].GetInt() - 1;

      _layers[i].push_back({tileValue % (int)tilesetSize.width,
                            tileValue / (int)tilesetSize.width});
    }
  }
}

const std::vector<npmw::Vec2Pixel>&
Map::layer(size_t z)
{
  return (_layers.at(z));
}

const npmw::Vec2Pixel
Map::sizeInTiles() const
{
  return {(int)_width, (int)_heigth};
}