#include <iostream>
#include <memory>

#include <SFML/Graphics.hpp>
#include <SDL_events.h>

#include "tileset.h"
#include "map.h"
#include "content.h"

#include "render_targets/dxd3/dxwindow.h"
#include "render_targets/dxd3/dxrendertarget.h"
#include "render_targets/dxd3/dxsprite.h"
#include "render_layer/layerrenderer.h"
#include "defs.h"

int
main() {
  npmw::dx::DXWindow window{GetModuleHandle(NULL), (HINSTANCE)0,
                            GetCommandLine(), SW_SHOWNORMAL, {1366, 768}};
  npmw::dx::DXRenderTarget renderTarget;
  renderTarget.initialize(window.getWindowHandle());

  HRESULT res;
  IDirect3DTexture9 *seasonsTex = nullptr;
  res = D3DXCreateTextureFromFile(renderTarget.device(),
                                  "Content/Seasons.png",
                                  &seasonsTex);

  npmw::dx::DXSprite spr{npmw::Rect<npmw::pixel>{{1366 / 2, 768 / 2},
                                                 {16*2, 16*2}},
                         npmw::Rect<npmw::pixel>{4*16, 8*16, 16*2, 16*2},
                         &seasonsTex,
                         renderTarget};

  /*npmw::LayerRenderer layer{(uint32_t)768 / npmw::tileSize.height,
                            (uint32_t)1366 / npmw::tileSize.width,
                            npmw::RectPixel{0, 0, 1366, 768},
                            &seasonsTex,
                            renderTarget};*/

  while(window.isOpen()) {
    window.update();

    //renderTarget.renderEnqueue(layer);
    renderTarget.renderEnqueue(spr);
    renderTarget.render();
  }

  if(seasonsTex != nullptr) {
    seasonsTex->Release();
  }

  return 0;
}

/// TODO:
/// pela posição da camera mostrar os tiles visiveis do mapa, converte
/// a posição dela para pixels depois pega o tamanho da area de visão
/// e pega os tiles visiveis nessa area.

/// TODO:
/// objetos estaticos tem sua posição inalterada certo, e objetos moveis,
/// tentar ver uma forma de desenhar tudo isso fazendo a menor ordenação
/// possivel, usar uma especie de matriz de adjacencia seria muito
/// interessante, essa matriz de adjacencia nao é cada posição um nó
/// é cada personagem um nó, é interessante ela saber ou existir um maximo
/// de nós qe ela teria,

/// TODO:
/// Fazer aquela sacada da ponte do zelda minish-cap.
