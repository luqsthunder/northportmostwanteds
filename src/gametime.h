#ifndef NPMW_GAME_TIME_HH
#define NPMW_GAME_TIME_HH

#include <SFML/System.hpp>

namespace npmw
{

class GameTime
{
public:
  GameTime();

  void update();

  void pause();
  const sf::Time& timeElapsed() const;
  const sf::Uint32 framesElapsed() const;

private:
  sf::Uint32 _frames;
  sf::Time _currentTime;
  sf::Time _lastPause;
  sf::Clock _gameTime;

  bool _paused;
};

}

#endif
