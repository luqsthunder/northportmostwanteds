#pragma once

#include <iostream>
#include <vector>
#include <utility>

#include <limits>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <glbinding/gl/gl.h>
#include <glbinding/gl/enum.h>

#include "defs.h"

namespace npmw { namespace gl4p {

struct GLVertex {
public:
  GLVertex();

  GLVertex(const glm::vec3 &pos, const glm::vec3 &color);

  GLVertex(glm::vec3 &&pos, glm::vec3 color);

  glm::vec3 pos;
  glm::vec3 color;
};


struct GLTexVertex {
  GLTexVertex() : tex(0.f, 0.f) { }
  GLTexVertex(const glm::vec2 &t) : tex(t) { }

  glm::vec2 tex;
};

template<typename TYPE>
uint32_t createGLBuffer(gl::GLenum bufferType, gl::GLenum usage,
                        TYPE *array, size_t dataSizeBytes,
                        bool unbind = true) {
  using namespace gl;
  uint32_t vbo = 0;

  glGenBuffers(1, &vbo);
  if(dataSizeBytes != 0) {
    glBindBuffer(bufferType, vbo);
    glBufferData(bufferType, dataSizeBytes, array, usage);
  }

  if(unbind) {
    glBindBuffer(bufferType, 0);
  }

  return vbo;
}

template<typename T>
class GPUBuffer {
  struct GPUBufferIterator {
    typename std::vector<T>::iterator iter;
    GPUBuffer<T>  *m_buffer;

    inline ~GPUBufferIterator() {
      if(m_buffer != nullptr) {
        using namespace gl;
        m_buffer->bind();
        glBufferSubData(m_buffer->bindTarget(), 0,
                        m_buffer->size() * sizeof(T), m_buffer->data());
        m_buffer->unbind();
      }
    }

    inline GPUBufferIterator& operator=(const GPUBufferIterator &it) {
      *iter = *it;
      return *this;
    }

    inline GPUBufferIterator& operator=(const T &it) {
      *iter = it;
      return *this;
    }

    inline bool operator != (const GPUBufferIterator &it) {
      return iter != it.iter;
    }

    inline GPUBufferIterator& operator ++() { ++iter; return *this;}
    inline const GPUBufferIterator operator ++(int) { iter++; return *this; }

    inline T& operator *() {return *iter; }
  };

public:
  inline explicit GPUBuffer(size_t elements = 0,
                            gl::GLenum bindTarget = gl::GL_ARRAY_BUFFER,
                            gl::GLenum usage = gl::GL_STATIC_DRAW)
    : _bind(false),
      _locked(false),
      _bindTarget(bindTarget),
      _lockRegion({0, 0}),
      _disposed(false),
      _vbo(createGLBuffer<T>(gl::GL_ARRAY_BUFFER, usage, nullptr, elements))
      { _gpuData.resize(elements); }

  inline explicit GPUBuffer(std::vector<T> &&data,
                            gl::GLenum usage = gl::GL_ARRAY_BUFFER,
                            gl::GLenum bufferFlag = gl::GL_STATIC_DRAW)
   : _bind(false),
     _locked(false),
     _bindTarget(usage),
     _lockRegion({0, 0}),
     _disposed(false),
     _gpuData(std::move(data)),
     _vbo(createGLBuffer(usage, bufferFlag, &data[0],
                         sizeof(T) * data.size())) { }

  inline explicit GPUBuffer(const std::vector<T> &data,
                            gl::GLenum usage = gl::GL_ARRAY_BUFFER,
                            gl::GLenum bufferFlag = gl::GL_STATIC_DRAW)
    : _bind(false),
      _locked(false),
      _bindTarget(usage),
      _lockRegion({0, 0}),
      _disposed(false),
      _gpuData(data),
      _vbo(createGLBuffer(usage, bufferFlag, &data[0],
                          sizeof(T) * data.size())) { }

  GPUBuffer(GPUBuffer&& ot) noexcept = default;
  GPUBuffer& operator=(GPUBuffer&& ot) noexcept = default;

  GPUBuffer(const GPUBuffer&) = delete;
  GPUBuffer& operator=(const GPUBuffer&) = delete;

  ~GPUBuffer() { dispose(); }

  void resize(size_t count) {

  }

  void resize(size_t count, const T &val) {

  }

  void reserve(size_t count) {

  }

  inline size_t size() { return _gpuData.size(); }
  inline size_t size() const { return _gpuData.size(); }

  inline void changeBindTarget(gl::GLenum bindTarget) noexcept {
    _bindTarget = bindTarget;
  }
  inline gl::GLenum bindTarget() const noexcept { return _bindTarget; }
  inline gl::GLenum bindTarget() noexcept { return _bindTarget; }

  inline void bind() {
    gl::glBindBuffer(_bindTarget, _vbo);
  }

  inline void unbind() {
    gl::glBindBuffer(_bindTarget, 0);
  }

  inline void dispose() {
    if(!_disposed) {
      gl::glDeleteBuffers(1, &_vbo);
      _disposed = true;
    }
  }

  inline const T * const data() const noexcept { return &_gpuData[0]; }
  inline const T * const data() noexcept { return &_gpuData[0]; }

  inline T* unlock(const size_t begin = 0, const size_t end = 0) noexcept {
    if(begin >= end) {
      return nullptr;
    }

    _lockRegion = std::tie(begin, end == 0 ? _gpuData.size() : end);
    return &_gpuData[begin];
  }

  inline void lock() {
    gl::glBindBuffer(_bindTarget, _vbo);
    gl::glBufferSubData(_bindTarget, sizeof(T) * _lockRegion.first,
                        sizeof(T) * (_lockRegion.second - _lockRegion.first),
                        &_gpuData[0]);
  }

  GPUBufferIterator begin() {
    return GPUBufferIterator{_gpuData.begin(), this};
  };

  GPUBufferIterator end() {
    return GPUBufferIterator{_gpuData.end(),
                             nullptr};
  };

private:
  friend GPUBufferIterator;

  bool _bind, _locked;
  gl::GLenum _bindTarget;
  uint32_t _vbo;
  std::pair<int, int> _lockRegion;
  std::vector<T> _gpuData;
  bool _disposed;
};

typedef GPUBuffer<unsigned> IndexBuffer;
typedef GPUBuffer<GLVertex> VertexBuffer;
typedef GPUBuffer<GLTexVertex> TexvertexBuffer;

class glimplRenderTarget;

class glimplTexture;

class glimplRenderable {
public:
  glimplRenderable(std::vector<GLVertex> &&vertices,
                   std::vector<GLTexVertex> &&texCoords,
                   glimplTexture * const texture,
                   std::vector<unsigned> &&indices,
                   glimplRenderTarget &renderTarget);

  virtual ~glimplRenderable();

  inline glimplTexture  *const texture() const {
    return m_texture != nullptr ? *m_texture : nullptr;
  }

  inline glimplTexture * texture() {
    return m_texture != nullptr ? *m_texture : nullptr;
  }

  inline const std::vector<GLVertex>& vertices() const { return _vertices; }
  inline const std::vector<GLVertex>& vertices() { return _vertices; }

  inline const std::vector<GLTexVertex>& texCoords() const { return _texCoords; }
  inline const std::vector<GLTexVertex>& texCoords() { return _texCoords; }

  inline const std::vector<unsigned>& indices() { return _indices; }
  inline const std::vector<unsigned>& indices() const { return _indices; }

  void updateTexture() { }
  void updateVertices() { }

protected:
  std::vector<GLVertex> _vertices;
  std::vector<GLTexVertex> _texCoords;
  std::vector<unsigned> _indices;
  glimplTexture ** m_texture;
private:
};

class glimplRenderTarget {

};

class glimplTexture {

};

class Texture {

};

class Renderable {
  Renderable();

  ~Renderable();

  inline const std::vector<npmw::gl4p::GLVertex>& vertices() {
    return impl->vertices();
  }

private:
  glimplRenderable *impl;
};

class RenderTarget {
public:
  RenderTarget();

  void enqueue(const npmw::gl4p::Renderable &r);

  void render();
};



} }
