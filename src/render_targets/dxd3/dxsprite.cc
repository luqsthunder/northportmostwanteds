#include "dxsprite.h"

using namespace npmw;
using namespace dx;

std::vector<npmw::dx::TexVertex>
createTexQuad(const Rect<pixel> &texRect, IDirect3DTexture9 *tex) {
  D3DSURFACE_DESC texDesc;
  tex->GetLevelDesc(0, &texDesc);

  Rect<float> norm{(float)texRect.x() / 480.f,
                   (float)texRect.y() / texDesc.Height,
                   (float)texRect.width / 480.f, 
                   (float)texRect.height / texDesc.Height};

  return {{norm.x() + 1.f / 512.f, norm.y() + norm.height},
          {norm.x() + norm.width - 1.f / 512.f, norm.y() + norm.height},
          {norm.x() + 1.f / 512.f, norm.y()},
          {norm.x() + norm.width - 1.f / 512.f, norm.y()}};
}

DXSprite::DXSprite(const Rect<pixel> &rect, const Rect<pixel> &tex,
                   IDirect3DTexture9 ** const texture,
                   DXRenderTarget &renderTarget):
  m_sprRect(rect), m_tex(tex),
  DXRenderable({{ {-1.0f,  1.0f,  0.0f}, {255, 255, 255, 255}},
                { { 1.0f,  1.0f,  0.0f}, {255, 255, 255, 255}},
                { {-1.0f, -1.0f,  0.0f}, {255, 255, 255, 255}},
                { { 1.0f, -1.0f,  0.0f}, {255, 255, 255, 255}}},
                createTexQuad(tex, *texture),
                texture,
                {2, 1, 0, 2, 3, 1}, renderTarget) {
  ap_transformStack->LoadIdentity();
  ap_transformStack->Scale((FLOAT)m_sprRect.width / 2.f,
                           (FLOAT)m_sprRect.height / 2.f, 0.f);
  ap_transformStack->Translate((FLOAT)m_sprRect.x(),
                               (FLOAT)m_sprRect.y(), 0.f);
}

DXSprite::DXSprite(const Rect<pixel> &rect, const Rect<pixel> &tex,
                   IDirect3DTexture9 ** const texture,
                   uint8_t amountFrames,
                   DXRenderTarget &renderTarget): 
  DXSprite(rect, tex, texture, renderTarget) {

}