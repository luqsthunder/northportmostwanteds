#pragma once

#include <d3d9.h>
#include <d3dx9math.h>
#include <Windows.h>
#include "defs.h"

namespace npmw { namespace dx {

class DXRenderTarget;

class DXWindow
{
public:
  DXWindow(HINSTANCE hInstance, HINSTANCE hPrevInstance,
           LPTSTR lpCmdLine, int nCmdShow, const npmw::Vec2Pixel size);

  bool isOpen() const;
  void close();

  HWND getWindowHandle() { return _hWnd; }

  void update();
protected:
  HWND _hWnd;

  bool _open;
};

}
} // end of namespaces
