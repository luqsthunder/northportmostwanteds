#include "dxrenderable.h"

#include <d3dx9math.h>

#include "render_targets/dxd3/dxrendertarget.h"
#include "defs.h"

using namespace npmw;
using namespace dx;

template<typename T>
void safeRelease(T *ptr) {
  if(ptr != nullptr) {
    ptr->Release();
  }
}


DXRenderable::DXRenderable(std::vector<Vertex> &&vertices,
                           std::vector<TexVertex> &&texCoords,
                           IDirect3DTexture9 ** const texture,
                           std::vector<WORD> &&indices,
                           DXRenderTarget &renderTarget):
  _vertices(vertices), _indices(indices), _texCoords(texCoords),
  ap_vbTexCoords(nullptr), ap_vbVertices(nullptr), ap_indexBuffer(nullptr), 
  nm_Texture(texture) {

  if(!initialize(renderTarget)) {
    std::cout << "erro on constructor initializing buffers \n";
  }

  HRESULT res = D3DXCreateMatrixStack(0, &ap_transformStack);
  if(FAILED(res)) {
    std::cout << "erro on constructor creating matrix stack \n";
  }
}

DXRenderable::DXRenderable(const std::vector<Vertex> &vertices,
                           const std::vector<TexVertex> &texCoords,
                           IDirect3DTexture9 ** const texture,
                           const std::vector<WORD> &indices,
                           DXRenderTarget &renderTarget):
  _vertices(vertices), _indices(indices), _texCoords(texCoords), 
  ap_vbTexCoords(nullptr), ap_vbVertices(nullptr), ap_indexBuffer(nullptr), 
  nm_Texture(texture) {

  if(!initialize(renderTarget)) {
    safeRelease(ap_vbVertices);
    safeRelease(ap_indexBuffer);
    std::cout << "erro on constructor \n";
  }

  HRESULT res = D3DXCreateMatrixStack(0, &ap_transformStack);
  if(FAILED(res)) {
    std::cout << "erro on constructor creating matrix stack \n";
  }
}

bool
DXRenderable::initialize(DXRenderTarget &renderTarget) {

  /*for(size_t i = 0; i < _texCoords.size(); ++i) {
    _vertices[i].tex(_texCoords[i].u, _texCoords[i].v);
  }*/

  HRESULT res = renderTarget.device()
    ->CreateVertexBuffer(sizeof(npmw::dx::Vertex) * _vertices.size(),
                         0, npmw::dx::Vertex::vertexFormat(), D3DPOOL_MANAGED,
                         &ap_vbVertices, nullptr);
  if(FAILED(res)) {
    std::cout << "cannot create vertexbuffer for this renderable \n";
    return false;
  }

  res = renderTarget.device()
    ->CreateVertexBuffer(sizeof(npmw::dx::TexVertex) * _texCoords.size(),
                         0, npmw::dx::TexVertex::vertexFormat(), 
                         D3DPOOL_MANAGED, &ap_vbTexCoords, nullptr);
  if(FAILED(res)) {
    std::cout << "cannot create vertexbuffer for texture in this renderable \n";
    return false;
  }

  res = renderTarget.device()
    ->CreateIndexBuffer(sizeof(WORD) * _indices.size(),
                        D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED,
                        &ap_indexBuffer, nullptr);

  if(FAILED(res)) {
    std::cout << "cannot create vertexbuffer for this renderable \n";
    return false;
  }

  npmw::dx::Vertex *ptrVbData = nullptr;
  res = ap_vbVertices->Lock(0, 0, (VOID**)&ptrVbData, 0);
  if(FAILED(res)) {
    std::cout << "cannot lock vb Renderable\n";
    return false;
  }
  std::copy(_vertices.begin(), _vertices.end(), ptrVbData);
  res = ap_vbVertices->Unlock();
  if(FAILED(res)) {
    std::cout << "cannot unlock VB Renderable \n";
    return false;
  }

  npmw::dx::TexVertex *ptrVbTexData = nullptr;
  res = ap_vbTexCoords->Lock(0, 0, (VOID**)&ptrVbTexData, 0);
  if(FAILED(res)) {
    std::cout << "cannot lock vb tex Renderable\n";
    return false;
  }
  std::copy(_texCoords.begin(), _texCoords.end(), ptrVbTexData);
  res = ap_vbTexCoords->Unlock();
  if(FAILED(res)) {
    std::cout << "cannot unlock vb tex Renderable \n";
    return false;
  }

  WORD *ptrIndexData = nullptr;
  res = ap_indexBuffer->Lock(0, 0, (VOID**)&ptrIndexData, 0);
  if(FAILED(res)) {
    std::cout << "cannot lock IB Renderable\n";
    return false;
  }
  std::copy(_indices.begin(), _indices.end(), ptrIndexData);
  res = ap_indexBuffer->Unlock();
  if(FAILED(res)) {
    std::cout << "cannot unlock IB Renderable \n";
    return false;
  }

  return true;
}

DXRenderable::~DXRenderable() {
  safeRelease(ap_vbVertices);
  safeRelease(ap_vbTexCoords);
  safeRelease(ap_indexBuffer);
  safeRelease(ap_transformStack);
}