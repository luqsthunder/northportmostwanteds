#pragma once

#include <queue>

#include <d3d9.h>
#include <d3dx9math.h>
#include <Windows.h>

namespace npmw { namespace dx {

class DXRenderable;

class DXRenderTarget
{
public:
  DXRenderTarget();

  ~DXRenderTarget();

  void initialize(HWND _hWnd);

  void renderEnqueue(const npmw::dx::DXRenderable &r);

  void render();

  inline IDirect3DDevice9* device() { return ap_dxDevice; }

private:
  bool _initialized;

  std::queue<const DXRenderable *> m_queueRender;
  PDIRECT3DVERTEXDECLARATION9 ap_vertexDecl;
  IDirect3D9 *ap_D3D9;
  IDirect3DDevice9 *ap_dxDevice;
};

}
}