#include "dxrendertarget.h"

#include <iostream>

#include "dxrenderable.h"

using namespace npmw;
using namespace dx;

template<typename T>
void safeRelease(T *ptr) {
  if(ptr != nullptr) {
    ptr->Release();
  }
}

D3DVERTEXELEMENT9 VertexDec[] = {
  {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, 
   D3DDECLUSAGE_POSITION, 0},

  {0, sizeof(float) * 3, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, 
   D3DDECLUSAGE_COLOR, 0},

  {1, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, 
   D3DDECLUSAGE_TEXCOORD, 0},

   D3DDECL_END()
};

DXRenderTarget::DXRenderTarget(): _initialized(false) {
}

void
DXRenderTarget::initialize(HWND _hWnd) {
  if(_initialized)
    return;

  ap_D3D9 = Direct3DCreate9(D3D_SDK_VERSION);
  if(ap_D3D9 == nullptr) {
    MessageBox(0, "Direct3DCreate9() � Failed", 0, 0);
    exit(0);
  }

  D3DPRESENT_PARAMETERS D3Dpp;
  ZeroMemory(&D3Dpp, sizeof(D3Dpp));

  D3DDISPLAYMODE g_displayMode;

  ap_D3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &g_displayMode);

  D3DFORMAT adapterFormat = g_displayMode.Format;
  if(SUCCEEDED(ap_D3D9->CheckDeviceFormat(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
                                          adapterFormat,
                                          D3DUSAGE_DEPTHSTENCIL,
                                          D3DRTYPE_SURFACE, D3DFMT_D24S8))) {
    D3Dpp.AutoDepthStencilFormat = D3DFMT_D24S8;
  }
  else if(SUCCEEDED(ap_D3D9->CheckDeviceFormat(D3DADAPTER_DEFAULT,
                                               D3DDEVTYPE_HAL, adapterFormat,
                                               D3DUSAGE_DEPTHSTENCIL,
                                               D3DRTYPE_SURFACE,
                                               D3DFMT_D24X8))) {
    D3Dpp.AutoDepthStencilFormat = D3DFMT_D24X8;
  }
  else if(SUCCEEDED(ap_D3D9->CheckDeviceFormat(D3DADAPTER_DEFAULT,
                                               D3DDEVTYPE_HAL, adapterFormat,
                                               D3DUSAGE_DEPTHSTENCIL,
                                               D3DRTYPE_SURFACE,
                                               D3DFMT_D16))) {
    D3Dpp.AutoDepthStencilFormat = D3DFMT_D16;
  }
  else {
    return;
  }

  D3Dpp.Windowed = TRUE;
  D3Dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
  D3Dpp.hDeviceWindow = _hWnd;
  D3Dpp.BackBufferFormat = adapterFormat;
  D3Dpp.BackBufferWidth = 1366;
  D3Dpp.BackBufferHeight = 768;
  D3Dpp.EnableAutoDepthStencil = TRUE;

  ap_dxDevice = nullptr;
  HRESULT res = ap_D3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
                                      _hWnd,
                                      D3DCREATE_HARDWARE_VERTEXPROCESSING,
                                      &D3Dpp, &ap_dxDevice);
  if(FAILED(res)) {
    MessageBox(0, "CreateDevice() � Failed", 0, 0);
    ap_D3D9->Release();
    exit(-1);
  }

  res = ap_dxDevice->CreateVertexDeclaration(VertexDec, &ap_vertexDecl);
  
  if(FAILED(res)) {
    MessageBox(0, "CreateVertexDeclaration() � Failed", 0, 0);
    ap_D3D9->Release();
    exit(-1);
  }

  _initialized = true;
  res = ap_dxDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  res = ap_dxDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
  res = ap_dxDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

  res = ap_dxDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
  res = ap_dxDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
  res = ap_dxDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
  res = ap_dxDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
  res = ap_dxDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);

  //res = ap_dxDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
  //res = ap_dxDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
  /*res = ap_dxDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);*/

  res = ap_dxDevice->SetVertexDeclaration(ap_vertexDecl);
}

DXRenderTarget::~DXRenderTarget() {
  safeRelease(ap_dxDevice);
  safeRelease(ap_D3D9);
}

void
DXRenderTarget::renderEnqueue(const npmw::dx::DXRenderable &r) {
  m_queueRender.push(&r);
}

void
DXRenderTarget::render() {
  HRESULT res;

  res = ap_dxDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 
                           D3DCOLOR_XRGB(255, 255, 255), 1.0f, 0);
  res = ap_dxDevice->BeginScene();

  D3DXMATRIX matView;
  D3DXMatrixLookAtLH(&matView, 
                     &D3DXVECTOR3(1366.f / 2.f, 768.f / 2.f, 0.f),
                     &D3DXVECTOR3(1366.f / 2.f, 768.f / 2.f, -1.f),
                     &D3DXVECTOR3(0.f, -1.f, 0.f));
  res = ap_dxDevice->SetTransform(D3DTS_VIEW, &matView);

  D3DXMATRIX matProjection;
  D3DXMatrixOrthoLH(&matProjection, 1366.f, 768.f, 0.0f, 1.0f);

  res = ap_dxDevice->SetTransform(D3DTS_PROJECTION, &matProjection);

  while(!m_queueRender.empty()) {
    auto vbVerts = m_queueRender.front()->verticesVB();
    auto vbTex = m_queueRender.front()->textureVB();
    auto ib = m_queueRender.front()->indexBuffer();
    auto texture = m_queueRender.front()->texture();

    UINT primitiveCount = m_queueRender.front()->indices().size() / 3;
    UINT verticesCount = m_queueRender.front()->vertices().size();

    D3DXMATRIX *m = m_queueRender.front()->transform();
    res = ap_dxDevice->SetTransform(D3DTS_WORLD, m);

    if(texture == nullptr) {
      std::cout << "lolmen\n";
    }
    res = ap_dxDevice->SetTexture(0, texture);

    res = ap_dxDevice->SetStreamSource(0, vbVerts, 0, 
                                       sizeof(npmw::dx::Vertex));
    res = ap_dxDevice->SetStreamSource(1, vbTex, 0,
                                       sizeof(npmw::dx::TexVertex));
    res = ap_dxDevice->SetIndices(ib);
    res = ap_dxDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 
                                            verticesCount, 0, primitiveCount);
    m_queueRender.pop();
  }

  ap_dxDevice->EndScene();
  res = ap_dxDevice->Present(nullptr, nullptr, nullptr, nullptr);
  if(res == D3DERR_DEVICELOST) {
    std::cout << "device lost \n";
  }
}
