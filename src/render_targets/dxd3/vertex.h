#pragma once

#include <d3d9.h>
#include <d3dx9math.h>
#include <Windows.h>

namespace npmw { namespace dx {

class Vertex {
public:
  Vertex() noexcept: Vertex({0, 0, 0}, {0, 0, 0, 255}) { }

  Vertex(const D3DXVECTOR3 &pos, const D3DXCOLOR &c) noexcept
    : _pos(pos), _color(D3DCOLOR_XRGB((uint8_t)c.r, (uint8_t)c.g,
                                        (uint8_t)c.b)), u(0), v(0) { }

  static DWORD vertexFormat() { return (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1); }

  inline DWORD color() const noexcept { return _color; }
  inline D3DXVECTOR3 pos() const noexcept { return _pos; }

  inline void pos(const D3DXVECTOR2 &pos) noexcept { _pos = {pos.x, pos.y, _pos.z}; }

  inline DWORD color() noexcept { return _color; }
  inline D3DXVECTOR3 pos() noexcept { return _pos; }

  inline void tex(float x, float y) { u = x; v = y; }

  inline void pos(const D3DXVECTOR3 &pos) noexcept { _pos = pos; }
  inline void color(const DWORD &color) noexcept { _color = color; }

  inline void color(const D3DXCOLOR &c) noexcept {
    _color = D3DCOLOR_XRGB((uint8_t)c.r, (uint8_t)c.g, (uint8_t)c.b);
  }

protected:
  D3DXVECTOR3 _pos;
  DWORD _color;
  float u, v;
};

class TexVertex {
public:
  TexVertex() noexcept: TexVertex(0.f, 0.f) { }
  TexVertex(float u1, float v1) noexcept: u(u1), v(v1) { }

  static DWORD vertexFormat() { return (D3DFVF_TEX1); }

  float u;
  float v;
};

}
}