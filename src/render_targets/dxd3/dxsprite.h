#pragma once

#include "dxrenderable.h"
#include "defs.h"

namespace npmw { namespace dx {

//!
//! Description of this class
//!
class DXSprite : public DXRenderable
{
public:
  DXSprite(const Rect<pixel>& sprRect, const Rect<pixel> &tex,
           IDirect3DTexture9 ** const texture,
           DXRenderTarget &renderTarget);

  DXSprite(const Rect<pixel>& rectTotal, const Rect<pixel> &tex,
           IDirect3DTexture9 ** const texture, uint8_t amountFrames, 
           DXRenderTarget &renderTarget);
protected:
  Rect<pixel> m_sprRect, m_tex;
};

}
}