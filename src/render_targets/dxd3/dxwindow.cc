#include "dxwindow.h"

#ifdef _WIN32
#define D3D_DEBUG_INFO

#include <iostream>

using namespace npmw;
using namespace dx;

LRESULT CALLBACK
windowCallback(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
  switch(message) {
    case WM_DESTROY:
      PostQuitMessage(0);
      return 0;
    case WM_PAINT:
      // Render a frame then validate the client area
      ValidateRect(hWnd, 0);
      return 0;
  }

  return DefWindowProc(hWnd, message, wParam, lParam);
}

DXWindow::DXWindow(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPTSTR lpCmdLine, int nCmdShow, const npmw::Vec2Pixel size) 
  : _open(true) { 
  const std::string WindowName = "NorthPort MW";

  // Define the window 
  WNDCLASSEX wcex;
  wcex.cbSize = sizeof(WNDCLASSEX);
  wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wcex.lpfnWndProc = windowCallback;
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.hInstance = hInstance;
  wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
  wcex.hCursor = LoadCursor(hInstance, IDC_ARROW);
  wcex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  wcex.lpszMenuName = NULL;
  wcex.lpszClassName = WindowName.c_str();
  wcex.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

  // Register the window 
  if(!RegisterClassEx(&wcex)) {
    auto lerr = GetLastError();
    MessageBox(0, "RegisterClassEx– Failed", 0, 0);
    exit(0);
  }

  // Create the window 
  _hWnd = CreateWindow(WindowName.c_str(), WindowName.c_str(),
                       WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
                       size.x, size.y, 0, 0, hInstance, 0);

  auto err = GetLastError();

  RECT rect = {0, 0, size.x, size.y};
  AdjustWindowRect(&rect, GetWindowLong(_hWnd, GWL_STYLE), FALSE);
  SetWindowPos(_hWnd, 0, 0, 0, rect.right - rect.left,
               rect.bottom - rect.top, SWP_NOZORDER | SWP_NOMOVE);

  ShowWindow(_hWnd, SW_SHOW);
  UpdateWindow(_hWnd);
}

void
DXWindow::close() {
  _open = false;
}

void
DXWindow::update() {
  MSG msg;
  while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
    if(msg.message == WM_QUIT) {
      _open = false;
    }

    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
}

bool
DXWindow::isOpen() const {
  return _open;
}

#endif
