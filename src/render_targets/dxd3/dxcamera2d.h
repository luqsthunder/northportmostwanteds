#pragma once

#include <d3dx9math.h>

namespace npmw { namespace dx {

class Camera2D {
  Camera2D();

  void move(D3DXVECTOR2& worldPos);

  void zoom(float zm);
  const float zoom() const;
  const float zoom();

  inline D3DXMATRIX &const view() { return _view; }
protected:
  D3DXMATRIX _view;
};

}
}