#pragma once

#include <queue>
#include <vector>

#include "vertex.h"

namespace npmw { namespace dx {

class Vertex;
class DXRenderTarget;

class DXRenderable {
public:
  DXRenderable(std::vector<Vertex> &&vertices, 
               std::vector<TexVertex> &&texCoords, 
               IDirect3DTexture9 ** const texture,
               std::vector<WORD> &&indices, DXRenderTarget &renderTarget);

  DXRenderable(const std::vector<Vertex> &vertices,
               const std::vector<TexVertex> &texCoords,
               IDirect3DTexture9 **texture,
               const std::vector<WORD> &indices, DXRenderTarget &renderTarget);

  virtual ~DXRenderable();

  inline IDirect3DTexture9 *const texture() const {
    return nm_Texture != nullptr ? *nm_Texture : nullptr; 
  }

  inline IDirect3DTexture9 * texture() {
    return nm_Texture != nullptr ? *nm_Texture : nullptr;
  }

  inline const std::vector<Vertex>& vertices() const { return _vertices; }
  inline const std::vector<Vertex>& vertices() { return _vertices; }

  inline const std::vector<TexVertex>& texCoords() const { return _texCoords; }
  inline const std::vector<TexVertex>& texCoords() { return _texCoords; }

  inline const std::vector<WORD>& indices() { return _indices; }
  inline const std::vector<WORD>& indices() const { return _indices; }

  inline D3DXMATRIX *const transform() const { return ap_transformStack->GetTop(); }
  inline D3DXMATRIX * transform() { return ap_transformStack->GetTop(); }

  void updateTexture() { }
  void updateVertices() { }

  inline IDirect3DVertexBuffer9 *const verticesVB() {
    return ap_vbVertices;
  }

  inline IDirect3DVertexBuffer9 *const verticesVB() const {
    return ap_vbVertices;
  }

  inline IDirect3DVertexBuffer9 *const textureVB() {
    return ap_vbTexCoords;
  }

  inline IDirect3DVertexBuffer9 *const textureVB() const {
    return ap_vbTexCoords;
  }

  inline IDirect3DIndexBuffer9 *const indexBuffer() {
    return ap_indexBuffer;
  }

  inline IDirect3DIndexBuffer9 *const indexBuffer() const {
    return ap_indexBuffer;
  }

protected:
  std::vector<Vertex> _vertices;
  std::vector<TexVertex> _texCoords;
  std::vector<WORD> _indices;

  ID3DXMatrixStack *ap_transformStack;

  IDirect3DVertexBuffer9 *ap_vbVertices;
  IDirect3DVertexBuffer9 *ap_vbTexCoords;

  IDirect3DTexture9 **nm_Texture;

  IDirect3DIndexBuffer9 *ap_indexBuffer;
private:
  bool initialize(DXRenderTarget &renderTarget);
};

}
}