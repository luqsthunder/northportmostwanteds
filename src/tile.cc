#include "tile.h"

#include <iostream>

using namespace npmw;

Tile::Tile() : _tilesetTexture(nullptr) {}

Tile::Tile(std::shared_ptr<sf::Texture> tilesetTexture) :
  Tile(tilesetTexture, 1, 1, sf::Vector2f())
{
}

Tile::Tile(std::shared_ptr<sf::Texture> tilesetTexture,
           sf::Vector2f texPosInTiles) : Tile(tilesetTexture, 1, 1, 
                                              texPosInTiles)
{
}


Tile::Tile(std::shared_ptr<sf::Texture> tilesetTexture,
                          uint32_t frequency, uint32_t frames,
                          sf::Vector2f texPosInTiles) :
   _tilesetTexture(tilesetTexture)
{
  Tile::frequency(frequency);
  Tile::frames(frames);

  _freqCount = 0;
  _currentFrame = 0;

  _firstTileFrame = texPosInTiles;

  updateQuad(texPosInTiles);
}

uint32_t
Tile::frames() const
{
  return _frames;
}

const npmw::Vec2Pixel
Tile::size() const
{
  return {16, 16};
}

void
Tile::frames(uint32_t frame)
{
  _frames = std::max((uint32_t)1, frame);
}

uint32_t
Tile::frequency() const
{
  return _frequency;
}

void
Tile::frequency(uint32_t freq)
{
  _frequency = std::max((uint32_t)1, freq);
}

void
Tile::updateQuad(sf::Vector2f posInTiles)
{
 // npmw::FloatRect framePos(posInTiles.x * size().x,
 //                          posInTiles.y * size().y,
 //                          size().x, size().y);
}

void
Tile::update(const npmw::GameTime &gameTime)
{
  if((_freqCount == (_frequency - 1)) && (_frames > 1))
  {
    _currentFrame = (gameTime.framesElapsed() / _frequency) % _frames;
    updateQuad({_firstTileFrame.x + _currentFrame, _firstTileFrame.y});
  }
  _freqCount = gameTime.framesElapsed() % _frequency;
}

std::shared_ptr<sf::Texture>
Tile::texture() const
{
   return _tilesetTexture;
}

sf::Vector2f
Tile::firstFramePos() const
{
  return _firstTileFrame;
}

sf::Vector2f
Tile::currentFramePos() const
{
  return {_firstTileFrame.x + _currentFrame, _firstTileFrame.y};
}

bool
operator<(const Tile &left, const Tile &right)
{
  return (left.firstFramePos().y == right.firstFramePos().y ?
          left.firstFramePos().x < right.firstFramePos().x :
          left.firstFramePos().y < right.firstFramePos().y);
}


/*!
 * This operator sets which tile share the same area, they are equal, so any
 * tile which share frames(one inside frame area of other) will be considered
 * equal,  because these are to avoid duplicates on tileset class.
 */
bool
operator==(const Tile &left, const Tile &right)
{
  npmw::Rect<uint32_t> leftFrames((uint32_t)left.firstFramePos().x,
                                  (uint32_t)left.firstFramePos().y,
                                  (uint32_t)left.frequency(), 1);

  npmw::Rect<uint32_t> rightFrames((uint32_t)right.firstFramePos().x, 
                                   (uint32_t)right.firstFramePos().y,
                                   (uint32_t)right.frequency(), 1);

  return ((leftFrames.intersects(rightFrames)) &&
          (left.texture() == right.texture()));
}

std::ostream& operator<<(std::ostream& out, const npmw::Tile& tile)
{
  out << "Frames" << tile.frames() << ": frequency" << tile.frequency()
      << ": texPos[" << tile.firstFramePos().x << tile.firstFramePos().y << "]";
  return out;
}
