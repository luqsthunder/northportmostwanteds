#pragma once

#include <unordered_map>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <iostream>
#include <string>

#include <boost/filesystem.hpp>

namespace npmw
{
//!
//! Descritor da ordem dos atributos a serem lidos/escritos para uma stream
//!
class StreamObjDescriptor
{
public:

  //!
  //! Essa funcao retorna o valor em bytes de cada atributo descrito na
  //! stream. e.g: uma classe que deseja que suas instancias sejam salvas em
  //! arquivo deve implementar essa funcao com os valores em sequencia de
  //! seus respectivos atributos menbros a serem salvos.
  //! Sim isso pode levar a quebra o emcapsulamento.
  //!
  virtual const std::vector<uint8_t>& f() const = 0;

  //!
  //! Retorna o nome de cada attributo a ser salvo, na ordem de serem salvos
  //!
  virtual const std::vector<std::string>& attributeNames() const = 0;
};


template<typename T>
class ContentManager
{
public:
  ContentManager(std::vector<std::string> paths,
                 std::vector<std::string> extensions,
                 void (*loadFun)(T&, const std::string &))
  {
    for(const auto & path : paths)
    {
      bool addFile = false;
      boost::filesystem::path p{path};

      boost::filesystem::directory_iterator end_itr;
      std::string currentFile;
      for(boost::filesystem::directory_iterator itr(p); itr != end_itr; ++itr)
      {
        if(boost::filesystem::is_regular_file(itr->path()))
        {
          currentFile = itr->path().string();

          //! verifing if has no extension required for files.
          if(extensions.empty())
          {
            for(const auto &i : currentFile)
            {
              if(i == '.')
                addFile = true;
            }
          }

          //! verifing if current file attend any extension required
          for(const auto &ext : extensions)
          {
            std::string fSubStr =
              currentFile.substr(currentFile.size() - ext.size());
            if(fSubStr == ext)
              addFile = true;
          }
        }

        if(addFile)
        {
          addFile = false;
          auto currContentPtr = std::make_shared<T>(T{});
          loadFun(*currContentPtr.get(), currentFile);
          auto strName = currentFile.substr(path.size() + 1);
          _content.emplace(strName, currContentPtr);
        }
      }
    }
  }

  const std::shared_ptr<T> 
  item(const std::string &name) const
  {
    auto resIt = _content.find(name);

    if(resIt == _content.end())
      throw std::range_error("name not found");

    return resIt->second;
  }

protected:
  std::unordered_map<std::string, std::shared_ptr<T>> _content;
};

}
