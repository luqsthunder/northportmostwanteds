#ifndef __NPMW_TILESET_H__
#define __NPMW_TILESET_H__
#include <memory>

#include <SFML/Graphics/Texture.hpp>

#include "tile.h"

namespace npmw
{

class Tileset
{
public:
  Tileset(std::shared_ptr<sf::Texture> texture);
  Tileset(const std::fstream &stream);

  void load(const std::fstream &stream);

  void changeTile(uint pos, uint frequency, uint frames,
                  sf::Vector2f texPosInTiles);

  void insertTile(uint frequency, uint frames, sf::Vector2f texPosInTiles);
  void organize();

  npmw::Tile at(uint pos);
  npmw::Tile at(uint posx, uint poxy);

  std::shared_ptr<sf::Texture> texture() const;

  npmw::Vec2Pixel size();
protected:
  std::vector<Tile> _set;
  uint _lines;
  std::shared_ptr<sf::Texture> _texture;
};

}

#endif
