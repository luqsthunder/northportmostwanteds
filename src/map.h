#pragma once

#include <string>
#include <vector>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "content.h"
#include "defs.h"

namespace npmw
{

class Tile;

class Map : sf::NonCopyable
{
public:
  Map(const std::string &path, const npmw::SizeU32 tilesetSize);

  const std::vector<npmw::Vec2Pixel>& layer(size_t z);
  const npmw::Vec2Pixel sizeInTiles() const;

private:
  unsigned _width, _heigth;

  std::vector<uint32_t> _layerDepths;
  std::vector<std::vector<npmw::Vec2Pixel>> _layers;
};

}