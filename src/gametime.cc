#include "gametime.h"

using namespace npmw;

GameTime::GameTime() : _frames(0), _paused(false)
{
}

void
GameTime::update()
{
  if(! _paused)
  {
    ++_frames;
    _currentTime = _gameTime.getElapsedTime() + _lastPause;
  }
}

void
GameTime::pause()
{
  if(! _paused)
  {
     _lastPause = (_gameTime.getElapsedTime());
     _paused = true;
  }
  else
     _gameTime.restart();
}

const sf::Time&
GameTime::timeElapsed() const
{
   return _currentTime;
}

const sf::Uint32
GameTime::framesElapsed() const
{
  return _frames;
}
