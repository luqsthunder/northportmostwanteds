#include "tileset.h"

using namespace npmw;

Tileset::Tileset(std::shared_ptr<sf::Texture> texture) :
  _texture(texture)
{
  if(texture == nullptr)
    throw std::invalid_argument("texture ");

  auto textureSize = _texture->getSize();
  _lines = (textureSize.y / size().y);
  _set.push_back(npmw::Tile(texture));
}

npmw::Vec2Pixel
Tileset::size()
{
  auto _size = _texture->getSize();
  return {(int)_size.x, (int)_size.y};
}

Tileset::Tileset(const std::fstream &stream)
{
  load(stream);
}

void
Tileset::load(const std::fstream &stream)
{
  throw std::logic_error("not implemented m0th3rf4c3r");
}

void
Tileset::insertTile(uint frequency, uint frames,
                    sf::Vector2f texPosInTiles)
{
  npmw::Tile tile(_texture, frequency, frames, texPosInTiles);

  _set.push_back(tile);
}

npmw::Tile
Tileset::at(uint pos)
{
  return _set.at(pos);
}

inline
npmw::Tile
Tileset::at(uint posx, uint posy)
{
  return _set.at(posx + (posy * _lines));
}

std::shared_ptr<sf::Texture>
Tileset::texture() const
{
  return _texture;
}
