#pragma once

#include "rect.h"

#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>

namespace npmw {

typedef unsigned int uint;
typedef unsigned short ushort;

typedef Rect<int>    IntRect;
typedef Rect<float>  FloatRect;
typedef Rect<uint>   UintRect;
typedef Rect<ushort> UShortRect;

typedef int pixel;
typedef float meter;

typedef sf::Vector2<pixel> Vec2Pixel;
typedef sf::Vector3<pixel> Vec3Pixel;

typedef Rect<pixel> RectPixel;

template <typename T>
struct Size {
  Size() noexcept;
  constexpr Size(const T w, const T h) noexcept;

  T width;
  T height;
};

template<typename T>
Size<T>::Size() noexcept : Size(0, 0) { }

template<typename T>
constexpr Size<T>::Size(const T w, const T h) noexcept : width(w), height(h) { }

typedef Size<uint8_t> SizeU8;
typedef Size<uint32_t> SizeU32;
typedef Size<size_t> SizeU64;

constexpr SizeU8 tileSize{16, 16};
}

